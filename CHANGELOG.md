## [1.0.3+1] - 04/06/2021

* Update documentation comments.

## [1.0.3] - 25/05/2021

* Fix bug: Calculate value when clicked on star items.
* Improve documentation.
* Rename to RatingStars.

## [1.0.2] - 13/05/2021

* Remove flutter_svg.

## [1.0.1] - 13/05/2021

* First release version.
